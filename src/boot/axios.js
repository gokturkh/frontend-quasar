/* eslint-disable indent */
import Vue from 'vue'
import axios from 'axios'

export default ({ router, store }) => {
    Vue.prototype.$axios = axios
    axios.defaults.baseURL = 'http://api.quasar.test'
    axios.defaults.withCredentials = true

    axios.interceptors.response.use(
        function (response) {
            return response
        },
        function (error) {
            switch (error.response.status) {
                case 401: // Not logged in
                case 419: // Session expired
                    store.dispatch('auth/loginLogout', false)
                    store.dispatch('auth/setUser', null)
                    router.push({
                        path: '/login'
                    }, () => { })
                    break
                case 503: // Down for maintenance
                    // Bounce the user to the login screen with a redirect back
                    console.log('Down for maintenance.')
                    break
                case 500:
                    console.log('Oops, something went wrong!  The team have been notified.')
                    break
                default:
                    // Allow individual requests to handle other errors
                    return Promise.reject(error)
            }
        })
}
