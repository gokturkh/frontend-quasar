/* import Vue from 'vue'
import vuelidate from 'vuelidate'

Vue.prototype.$vuelidate = vuelidate */

import Vue from 'vue'
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)
