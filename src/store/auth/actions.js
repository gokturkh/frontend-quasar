export function loginLogout (context, isLoggedIn) {
  context.commit('updateIsLoggedIn', isLoggedIn)
}

export function setUser (context, user) {
  context.commit('setUser', user)
}

export function getUser (context) {
  context.commit('getUser')
}
