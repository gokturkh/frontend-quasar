/*
export function someMutation (state) {
}
*/

export const updateIsLoggedIn = (state, isLoggedIn) => {
  state.isLoggedIn = isLoggedIn
}

export const setUser = (state, user) => {
  state.user = user
}
